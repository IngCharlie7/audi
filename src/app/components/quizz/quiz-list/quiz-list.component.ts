import { Component, OnInit } from '@angular/core';

// model
import { QuizzData } from '../../../models/quizz';

// service
import { QuizService } from '../../../services/quiz.service';

// toastr
import { ToastrService } from 'ngx-toastr';
import { Company } from '../../../models/company';

@Component({
  selector: 'app-product-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.css']
})
export class QuizListComponent implements OnInit {
  public companyList: any = [];
  constructor(
    private quizService: QuizService,
    private toastr: ToastrService
  ) { 
    
  }

  ngOnInit() {
    this.companyList = this.quizService.getCompany();
    /*return this.quizService.getQuiz()
      .snapshotChanges().subscribe(item => {
        this.quizList = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x["$key"] = element.key;
          this.quizList.push(x as QuizzData);
        });
      });*/
  }

  onEdit(employee: QuizzData) {
    this.quizService.selectedquiz = Object.assign({}, employee);
  }

  onDelete(data) {
    console.log(data)
    if (confirm('Estas seguro de eliminar?')) {
      this.quizService.deleteCompany(data);
      this.toastr.warning('Eliminado Correcto', 'Empleado eliminado');
    }
  }

}
