import { Component } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "login",
  templateUrl: "login.component.html",
  styleUrls: ['login.component.css']
})
export class LoginComponent {
  public email: string;
  public password: string;
  public requiredEmail:boolean;
  constructor(
    public authService: AuthService,
    public router: Router,
    private toastr: ToastrService,
  ) { }

  keyPress(event: any) {
    const pattern = /[a-z,@]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
}
  onLogin() {
    /*this.authService.register(this.email, this.password)
    .then((res)=>{
      console.log("creado")
      console.log(res)
      //this.router.navigate(['/private'])
    }).catch((err)=>{
      console.log(err);
    })*/
    this.requiredEmail = true;
    this.authService.login(this.email, this.password)
    .then((res)=>{
      this.router.navigate(['/private'])
    }).catch((err)=>{
      this.toastr.warning('Datos Incorrectos', 'Administrador no encontrado');
      this.router.navigate(['/login'])
      
    })
  }
}
