import { Component, OnInit } from '@angular/core';
import { QuizService } from '../../../services/quiz.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public nameCompany: string;
  constructor(public employeeService: QuizService, private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  addCompany(companyForm: NgForm) {
    this.employeeService.insertCompany(companyForm.value);
    
    this.toastr.success('Acción Realizada', 'Empresa agregada');

  }
}
