import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Encuesta de Madurez a Empresas';
  public isLogin:boolean;
  public nombreUser:string;
  public emailUser:string;

  constructor(
    public authService:AuthService,
    public router: Router
  ){}

  ngOnInit(){
    this.authService.getAuth().subscribe(auth=>{
      if(auth){
        this.isLogin=true
        this.nombreUser=auth.displayName
        this.emailUser=auth.email
      }else{
        this.isLogin=false
      }
    })
  }
  logout(){
    this.authService.logout();
  }


}

